module.exports = XStreamAPI;


  /**
   * API Methods for encapsulating/reading XStream request/response packets
   *
   */

function XStreamAPI() {
  if ( !(this instanceof XStreamAPI) )
    return new XStreamAPI();
}


  /**
   * @private
   */



XStreamAPI.prototype.makeSenML = function(meta,data){
  var senML = [];

  var obj={};
  Object.keys(meta).forEach(function(key){
    if (['bn','pos','bt'].indexOf(key) >=0){
      obj[key] = meta[key];
    }
  });
  senML.push(obj);

  data.forEach(function(akey){
    obj={};
    Object.keys(akey).forEach(function(key){
      if (['n','u','v','min','max','vd','vi','vb','vs'].indexOf(key)>=0){
        obj[key] = akey[key];
      }
    });
    senML.push(obj);
  });

  return senML;
}


XStreamAPI.prototype.parseSenML = function(data){
  if (typeof(data) !='object' || data.length <2)
    return 0;

  meta= data[0];
  data._tags =[];

  data.items  = function() { return this.slice(1); };
  data.forEach = function(fn){
  	Object.keys(this).forEach((k)=> {
	if (typeof(this[k]) == 'object' && this[k].hasOwnProperty("n")) 
		fn(this[k]);})
  };	
  data.hasTag = function(t) { return (this._tags.indexOf(t) >=0); };
  data.urn  = function(){
      if (meta.hasOwnProperty('bn')) {
        return meta.bn;
      }
      return null;
    };

  for(var idx=1;idx<data.length;idx++){
    if (data[idx].hasOwnProperty('n')) {
      data._tags.push(data[idx].n);
    }

    data[idx].urn = data.urn;

   // urn:dev:xbee:0xXXXXX:nid:0123

    data[idx].nid = function(){
      if (meta.hasOwnProperty('bn') && meta.bn.split(":").length >=6) {
        return meta.bn.split(":")[5];
      }
      return null;
    };

    data[idx].xbeeAddr = function(){
      if (meta.hasOwnProperty('bn') && meta.bn.split(":").length >= 4) {
        return meta.bn.split(":")[3];
      }
      return null;
    };

    data[idx].pos = function(){
      if (meta.hasOwnProperty('pos') && meta.pos.length >=2){
        return [meta.pos[0], meta.pos[1]]; //long/lat
      }
      return null;
    };

    data[idx].name = function(){
      if (this.hasOwnProperty('n')) {
        return this.n;
      }
      else {
        return null;
      }
    };

    data[idx].valueBinary = function(){
      if (this.hasOwnProperty('vd')) {
        return this.vd;
      }
      return null;
    }

    data[idx].valueString = function(){
      if (this.hasOwnProperty('vs')) {
        return this.vs;
      }
      return null;
    }

    data[idx].valueBoolean = function(){
      if (this.hasOwnProperty('vb')) {
        return this.vb;
      }
      return null;
    }

    data[idx].value = function(){
      if (this.hasOwnProperty('v')) {
        return this.v;
      }
      return null;
    };
  }

  return data.length -1;
}

